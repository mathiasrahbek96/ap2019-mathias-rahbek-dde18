let bubbles = [];

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  bubbles[0] = new Bubble()
  frameRate(100);
}

function draw() {
  rectMode(CENTER);

//bubbles that draws various objects when crossing specific posititions
  for (let i = 0; i < bubbles.length; i++) {
    bubbles[i].move();
    bubbles[i].show();
    if (bubbles[i].y > height / 4 * 3 + 100) {
      canvasFrame();
      bubbles.push(new Bubble());
      bubbles.splice(i, 1);
      rdmRect();
    }
    if (bubbles[i].y < height / 4 - 100) {
      canvasFrame();
      bubbles.push(new Bubble());
      bubbles.splice(i, 1);
      rdmTriangle();
    }
    if (bubbles[i].x > width / 4 * 3 + 100) {
      canvasFrame();
      bubbles.push(new Bubble());
      bubbles.splice(i, 1);
      rdmEllipse();
    }
    if (bubbles[i].x < width / 4 - 100) {
      canvasFrame();
      bubbles.push(new Bubble());
      bubbles.splice(i, 1);
      rdmLine();
    }
  }
}

//the frame of the canvas
function canvasFrame() {
  noFill()
  strokeWeight(random(0.1, 0.3))
  stroke(0)
  rect(width / 2, height / 2, random(630, 770), random(330, 470))
}

// various objects/shapes that contribute to the expression of the painting

function rdmTriangle() {
  fill(random(130, 150), random(50));
  strokeWeight(random(0.1, 0.3))
  stroke(random(20))
  triangle(random(width / 4 - 50, width / 4 * 3 + 50), random(height / 4 - 50, height / 4 * 3 + 50), random(width / 4 - 50, width / 4 * 3 + 50), random(height / 4 - 50, height / 4 * 3 + 50), random(width / 4 - 50, width / 4 * 3 + 50), random(height / 4 - 50, height / 4 * 3 + 50))
}

function rdmLine() {
  fill(random(100))
  strokeWeight(random(0.1, 0.3))
  stroke(random(20))
  line(random(width / 4 - 50, width / 4 * 3 + 50), random(height / 4 - 50, height / 4 * 3 + 50), random(width / 4 - 50, width / 4 * 3 + 50), random(height / 4 - 50, height / 4 * 3 + 50))
}

function rdmEllipse() {
  fill(random(130, 150), random(50));
  strokeWeight(random(0.1, 0.3))
  stroke(random(20))
  ellipse(random(width / 4, width / 4 * 3), random(height / 4, height / 4 * 3), random(150, 300));
}

function rdmRect() {
  fill(random(130, 150), random(50));
  strokeWeight(random(0.1, 0.3))
  stroke(random(20))
  rect(random(width / 4, width / 4 * 3), random(height / 4, height / 4 * 3), random(80, 200), random(80, 200))
}
