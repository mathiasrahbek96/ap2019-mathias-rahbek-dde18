class Bubble {
  constructor(x, y, r) {
    this.x = width / 2;
    this.y = height / 2;
    this.r = random(3, 10);
  }

  move() {
    this.x = this.x + random(-20, 20);
    this.y = this.y + random(-20, 20);

  }
  show() {
    noStroke()
    fill(random(200), random(100), random(0));
    ellipse(this.x, this.y, this.r * 2);
  }
}
