![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx7/example.png)

[RUNME](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx7/empty-example/index.html)

**My coding process**
In our group, we chose to work individually for this mini exercise. I have made a program that can create a piece of generative artwork over time. It consists of these 3 rules and is executed without any interaction.


1.    An array of bubbles moving randomly and independently from the center of the canvas which creates a trail of different colors
2.    The area of which the bubbles can move is constrained to 2/4 of both the windowWidth and windowHeight. If they cross this “border” the bubbles array is spliced and recalled
3.    Also, whenever the bubbles cross this constrained “boarder” in either the top, bottom, left, or right side a new object/shape is added in form of either a line, triangle, rectangle or ellipse together with a big rectangle, that slightly varies in width and height, which acts as the canvas frame for the artwork

The end product is never the same, which gives you the chance of exploring something new every time you open it. The expression is very abstract, and I have chosen some colors which should connotate autumn. The program never ends, which also means that the art piece is never done and that you at some point may experience the piece becoming too messy and unorganized to look at. To practice the syntax from former weeks, I tried to implement as many functions possible including, class, arrays, for-loop, conditional statements, and more. It was a hurdle to get it to work the way I wanted especially with the collaboration between the array and class. But eventually, it worked out.

The general point of these implemented rules is to let the program create itself. I have used the random() syntax a lot to let the program behave on chance instead of strict parameters and without interaction. And to add to the discussion which we had in class week 12; it really is fascinating how we can create something that works independently under the given set of rules from the programmer. The fact that it basically becomes the machine that creates the art piece and that it does it “randomly” is very extraordinary. 

To reflect upon automatism and generativity I want to refer to a talk by Marius Watz where he borrows a definition on generative art by Philip Galanter.

"Generative art refers to any art practice where the artist uses a system, such as a set of natural language rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art." (Beautiful Rules: Generative models of creativity (2007) by Marius Watz) 

So, in this sense, there seems to be a connection between generativity and automatism. We as programmers are the ones responsible for this system being set into motion, thereby we also have the possibility of implementing an autonomy to certain degrees. We are the authors of the instructions for the computer to execute and therefore the generativity starts with us and is then exerted by the machine, which in my opinion, also make us the authors of generative art. 
So, automatism feeds generativity and contributes to the art piece taking shape. In my program I have implemented the automatism in terms of letting the painting draw itself over time with the use of a for-loop, a class, an array, conditional statements, various shapes and colors, and the random() syntax. This helps the program to generate visuals automatically within this set of rules. However, conditional statements can also be used as a hindrance for automatism; by giving the program a specific instruction (if-statement) that has to be accomplished before continuing with the rest of the program. But as I said earlier, that is a choice of action that lies with the programmer, and it depends on what purpose the program serves.

