var words = ["sleepy equality-moji", "excited equality-moji"]
var index = 0
var mic;
let amt, startColor, newColor;


function setup() {
  createCanvas(1100, windowHeight);
  frameRate(60);
  mic = new p5.AudioIn();
  mic.start();

  startColor = color(255, 255, 255);
  newColor = color(random(255), random(255), 0);
  amt = 0;

}

function draw() {
  background(255);
  var vol = mic.getLevel();

  push()

  //LEFT FACE//
  noStroke()
  fill(random(255), random(255), 0);
  ellipse(250, 300, 400, 400)

  //left eye
  fill(255)
  ellipse(160, 250, 80);

  fill(random(100), random(100), random(100));
  ellipse(160, 250, 30);

  //right eye
  fill(255)
  ellipse(340, 250, 80);

  fill(random(100), random(100), random(100));
  ellipse(340, 250, 30);

  //RIGHT FACE//
  noStroke()
  fill(random(255), random(255), 0);
  ellipse(850, 300, 400, 400)

  //left eye
  fill(255)
  ellipse(940, 250, 80);

  fill(random(100), random(100), random(100));
  ellipse(940, 250, 30);

  //right eye
  fill(255)
  ellipse(760, 250, 80);

  fill(random(100), random(100), random(100));
  ellipse(760, 250, 30);

  pop();


  fill(lerpColor(startColor, newColor, amt));
  amt += 0.01;
  if (amt >= 1) {
    amt = 0.0;
    startColor = newColor;
    newColor = color(random([200], [200]), random(255), random(255));
  }


  //LEFT FACE WITH LERP//
  noStroke()
  ellipse(250, 300, 400, 400)

  //RIGHT FACE WITH LERP//
  noStroke()
  ellipse(850, 300, 400, 400)

  //EYES//
  fill(0)

  //Left face left eye with interactivity
  ellipse(160, 250, 70, 5);
  ellipse(160, 250, 70, 200 * vol);

  //Left face right eye with interactivity
  ellipse(340, 250, 70, 5);
  ellipse(340, 250, 70, 200 * vol);

  //Right face left eye
  ellipse(770, 250, 70, 90);

  //Right face right eye
  ellipse(940, 250, 70, 90);

  push();
  translate(0, 200);
  //left face brows
  noFill()
  stroke(0)
  strokeWeight(2);
  //left brow
  beginShape();
  curveVertex(120, -20 * vol);
  curveVertex(120, -20 * vol);
  curveVertex(160, -170 * vol);
  curveVertex(200, -20 * vol);
  curveVertex(200, -20 * vol);
  endShape();
  //right brow
  beginShape();
  curveVertex(300, -20 * vol);
  curveVertex(300, -20 * vol);
  curveVertex(340, -170 * vol);
  curveVertex(380, -20 * vol);
  curveVertex(380, -20 * vol);
  endShape();
  pop()

  //sleepy mouth
  noStroke();
  ellipseMode(CENTER);
  ellipse(250, 390, 80, 10);
  ellipse(250, 390, 80, 350 * vol);

  //excited mouth default
  noFill()
  stroke(0)
  strokeWeight(10);
  beginShape();
  curveVertex(800, 400);
  curveVertex(800, 400);
  curveVertex(850, 415);
  curveVertex(900, 400);
  curveVertex(900, 400);
  endShape();

  //text sleepy equality-moji
  strokeWeight(2.6);
  textSize(32);
  text(words[index], 100, 600)

  strokeWeight(1);
  textSize(16);
  text('use your mic', 100, 565);

  //text excited equality-moji
  strokeWeight(2.6);
  textSize(32);
  text(words[index + 1], 730, 600)

  strokeWeight(1);
  textSize(16);
  text('hold any key', 730, 565)

  //interaction for the right emoji
  if (keyIsPressed === true) {
    noFill()
    stroke(0)
    strokeWeight(8);
    //left brow
    beginShape();
    curveVertex(740, 170);
    curveVertex(740, 170);
    curveVertex(770, 150);
    curveVertex(800, 170);
    curveVertex(800, 170);
    endShape();
    //right brow
    beginShape();
    curveVertex(910, 170);
    curveVertex(910, 170);
    curveVertex(940, 150);
    curveVertex(970, 170);
    curveVertex(970, 170);
    endShape();

    //excited mouth
    noStroke();
    fill(0);
    arc(850, 395, 110, 200, 0, PI)

    //Right face left eye
    ellipse(770, 250, 70, 100);

    //Right face right eye
    ellipse(940, 250, 70, 100);
  }


}
