![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx5/example1.png)
![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx5/example2.png)
[RUNME](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx5/empty-example/index.html)
NB: Use Chrome

**About my code**

In my mini ex 5, I’ve chosen to make a rework on my mini ex 2 which consists of what I’ve called equality-mojis. The purpose and concept is basically the same; to comply with the cultural and racial demands of emojis in the 21st century. I’ve done that by making emojis that changes skin color randomly. By including this broad looping color scheme, everyone is taken into account. And through the neutral and universal smiley shape which just comprises a head with two eyes and one mouth, you won’t differentiate in head and face structure. 
I thought that the color shifting of my mini ex 2 seemed too stiff and stammering so I tried using lerpColor in my mini ex 5, by the inspiration of Noah from my study group, and I am very satisfied with the outcome. And in this rework, I’ve also included interactivity through two capturing inputs, the microphone and the keyboard. Which, is the biggest difference of this rework.


*  The microphone controls the ‘sleepy equality-moji’ and is defined to the value of the width and height of the eyes and mouth, and also to the y-positions of the curveVertex in the eyebrows. It is meant to give the illusion of being startled when hearing a loud noise while sleeping.
*  The keyboard is used with the Boolean system variable keyIsPressed meaning that the value is true if any key is pressed and false if no keys are pressed. And when the value is true, eyebrows are added as curveVertex, an open mouth is added as an arc, and eyes are added as ellipses with a greater height. This emoji should, therefore, give the illusion of being excited when a key is pressed.

The purpose of this interactivity is basically to create a fun alternative for the default emoji. But also, to contribute further into the discourse of societal, cultural, and racial inclusions by giving the user even more possibilities in the emoji-library. And by giving the user a choice of interaction through data capturing inputs, you also place the responsibility on that user for the interaction to happen. This question of a ‘free’ choice is what should be in focus here. Because the world is going towards a new digital age wherein coding is becoming a literacy at the same heights as reading and writing. And you are almost left with no choice at all but to accept this destiny. Annette Vee writes about this in her book ‘Coding Literacy’.

*To add programming to our general concept of literacy means to enfold computational technology with print and other technologies of communication. What will this mean for people who are “noncoders?” Paul Ford writes, “If coders don’t run the world, they run the things that run the world.” (2017, p. 92)*

Society becomes more and more digitalized which means that programming and code become the new foundation of our world. You cannot stop this development unless the entire world agrees to. And by the fact that software has caused so much efficiency in societal, economic, and many other parameters, you can imagine that it has come to stay. However, this also leaves people with no other choice but to accept this fate and to ride along with the digital wave. Is it stretching it to say that it collides with human rights?
