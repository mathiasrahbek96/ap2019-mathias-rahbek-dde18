![ScreenShot](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx8/example1.png)
[RUNME](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx8/empty-example/index.html)

**About the program** 

For this mini ex, I worked with Noah from my study group. We made a program that could generate fairy tales automatically. To do this we wrote a template of a story and then mixed out the different verbs, nouns, and adjectives with words from a JSON file. Every time the program would be refreshed, a new combination of words would appear, and a new fairy tale would be generated.
Some tales make better sense than others. If we had the time, we could have chosen some more specified and sensemaking words for every replacement, but as a whole, we are quite satisfied with our work and thought it was a funny outcome. 


**The collaborative process**

In my opinion, we had a great working process and could easily collaborate with each other. Communication was key. We arranged what we wanted to do, by “sketching” up the program with notes, heard each other’s ideas, implemented the best, initialized the different tasks, and then arranged who did what. If we stumbled upon a problem, we talked about it, tried out different stuff and eventually figured it out. We also used the Teletype option in atom which worked very well. 


**Conceptual thoughts**

One thing that came to mind when working with this program was the fact that we are still the mind behind the machine. Sensemaking in our fairy tales was still up to us to define, which was manifested by the illogical and nonsense combinations of words it generated. Machines become more intelligent and work better and more efficiently for every day that passes, but this just reminded me that it still lacks plain knowledge, interpretation, and decisiveness, which it gets from us – the minds behind it.

*“secondary notation, a pejorative term suggesting that code's primary purpose is to be executed by a computer, and only secondarily to be understood by a human.”* [1]

As suggested here, machines’ primary purpose has always been to follow and execute instructions. To do what it is told. The human abilities to understand and interpret is basically what distinguishes us from them. Machines are mediums, empty shells that only can be filled out and used through human interference. When that said, I cannot blame the machine for generating nonsense fairy tales, because I haven’t learned it to interpret and understand. And for me to wholly understand the vast literacy of code is what limits me from making it understand me. There seems to be a linguistic barrier. But the paradox is that I am the one defining what I cannot understand.

1.	Cox, Geoff & McLean, Alex. “Vocable Code”. Speaking Code: Coding as Aesthetic and Political Expression. MIT Press, 2013. p. 23.
