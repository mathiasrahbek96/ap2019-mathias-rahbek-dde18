var basketballs = [];

// Scoreboard Variable
let score = 0;


function setup() {
  createCanvas(windowWidth, 700)
  basketballs[0] = new Basketball()

}


function draw() {
  background(200, 150, 100)
  rectMode(CENTER);

  //Live scoreboard
  textSize(50);
  fill(230, 180, 130)
  textAlign(CENTER, CENTER);
  text("" + score, width / 2, height / 2);

  //Spawn of basketballs
  //balls in front of backboard but behind the basketNet
  backboard();
  for (let i = 0; i < basketballs.length; i++) {
    basketballs[i].move();
    basketballs[i].show();
    if (basketballs[i].ypos > height) {
      basketballs.push(new Basketball());
      basketballs.splice(i, 1);
    }
  }
  basketNet();


  //hit and score
  if (basketballs[0].ypos + 10 >= height && basketballs[0].xpos <= mouseX + 25 && basketballs[0].xpos >= mouseX - 25) {
    fill(255, 200, 150);
    rect(mouseX, 600, 90, 10, 10)
    score++
  } else {
    if (basketballs[0].ypos + 10 >= height)
      score = 0;
  }

}

function backboard() {
  //pole
  fill(100)
  rect(mouseX, 660, 20, 150)
  //pole shade
  noStroke()
  fill(130)
  rect(mouseX + 8, 660, 3, 150)

  //backboard
  noStroke()
  fill(0)
  rect(mouseX, 560, 170, 120, 3) //black frame
  stroke(255)
  strokeWeight(10)
  rect(mouseX, 560, 120, 70) //white inner frame
}

function basketNet() {
  //red basket frame
  strokeWeight(1)
  fill(200, 0, 0)
  noStroke()
  rect(mouseX, 600, 90, 10, 10)

  //red basket frame shade
  fill(255, 200, 150);
  rect(mouseX, 597, 82, 1, 10)

  //net
  strokeWeight(2)
  stroke(255, 150)
  line(mouseX - 40, 605, mouseX - 30, 655) //ver line 1 from left side
  line(mouseX - 20, 605, mouseX - 17, 655) //ver line 2 from left side
  line(mouseX, 605, mouseX, 655) //ver line 3 from left side
  line(mouseX + 20, 605, mouseX + 17, 655) //ver line 4 from left side
  line(mouseX + 40, 605, mouseX + 30, 655) //ver line 5 from left side
  line(mouseX - 37, 615, mouseX + 37, 615) //hor line 1 from top
  line(mouseX - 34, 630, mouseX + 34, 630) //hor line 2 from top
  line(mouseX - 31, 645, mouseX + 31, 645) //hor line 2 from top
}
