var words = ["happy equality-moji", "sad equality-moji"]
var index = 0

function setup() {
  createCanvas(1100, windowHeight);
  background(250, 250, 150);
}

function draw() {

  frameRate(10);

  push()

  //left face
  noStroke()
  fill(random(600), random(650), 0);
  ellipse(250, 300, 400, 400)

  //left eye
  fill(255)
  ellipse(160, 250, 80);

  fill(random(100), random(100), random(100));
  ellipse(160, 250, 30);

  //right eye
  fill(255)
  ellipse(340, 250, 80);

  fill(random(100), random(100), random(100));
  ellipse(340, 250, 30);

  //right face
  noStroke()
  fill(random(600), random(650), 0);
  ellipse(850, 300, 400, 400)

  //left eye
  fill(255)
  ellipse(940, 250, 80);

  fill(random(100), random(100), random(100));
  ellipse(940, 250, 30);

  //right eye
  fill(255)
  ellipse(760, 250, 80);

  fill(random(100), random(100), random(100));
  ellipse(760, 250, 30);

  pop();


  //text happy
  textSize(32);
  text(words[index], 100, 600)

  //text sad
  textSize(32);
  text(words[index + 1], 730, 600)


  //happy mouth

  //strokeWeight(5);
  //point(125,360);
  //point(180, 400);
  //point(250, 420);
  //point(320, 400);
  //point(370, 360);
  //strokeWeight(1);

  noFill();
  strokeWeight(10);
  beginShape();
  curveVertex(330, 360);
  curveVertex(370, 360);
  curveVertex(320, 400);
  curveVertex(250, 410);
  curveVertex(180, 400);
  curveVertex(125,360);
  curveVertex(175,360);
  endShape();

  //sad mouth

  //strokeWeight(5);
  //point(750, 400);
  //point(850, 370);
  //point(950,400);
  //strokeWeight(1);

  noFill();
  strokeWeight(10);
  beginShape();
  curveVertex(750, 400);
  curveVertex(750, 400);
  curveVertex(850, 370);
  curveVertex(950,400);
  curveVertex(950,400);
  endShape();

}
