function setup() {
  createCanvas(1600, 800, WEBGL);

}

function draw() {
  translate(mouseX-width/2, mouseY-height/2);
  background(0);

  //sun
  fill(400, 200, 0);
  ellipse(320, 230, 20, 20);

  //earth
  rotateX(frameCount * 0.0001);
  rotateY(frameCount * 0.001);
  fill(0, 100, 0);
  strokeWeight(1)
  stroke(0, 80, 0);
  ellipsoid(200, 200);

  //bike
  stroke(20, 60, 70);
  rotateX(frameCount * 0.01);
  rotateZ(frameCount * 0.01);
  fill(100);

  //bike frame
  line(40, 210, 60, 240);
  line(100, 190, 70, 200);
  line(100, 190, 90, 225);
  line(70, 200, 90, 225);
  line(90, 225, 60, 240);
  line(60, 240, 70, 200);
  //wheels
  ellipse(40, 210, 30, 30);
  ellipse(100, 190, 30, 30);
  //seat
  translate(92, 225);
  box(13, 2, 10);
  //handlebar
  translate(-30, 15);
  box(2, 2, 22);

}
