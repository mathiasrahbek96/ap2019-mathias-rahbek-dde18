![ScreenShot](FlowchartMX6.png)

[URL for miniex 6](https://gitlab.com/mathiasrahbek96/ap2019-mathias-rahbek-dde18/tree/master/MiniEx6 )

**Individual flowchart**

I chose to draw a flowchart of my miniex6 which was a small game built upon object-oriented programming. What I noticed was that when making the flowchart, it almost simplified the complexity of my source code – undermining all of the hard work that laid behind. The program was indeed simple to use, but it was quite difficult and challenging to write. 
But the difficulty of drawing a flowchart is definitely to include the most necessary processes and functions of a program and to make it understandable. Another aspect is how technical the description must be, which at most depends on the knowledge of the person reading it. I have tried to make the description of my flowchart very simple for everyone to be able to read and understand it.

____

**Group work: flowcharts for final project ideas**



![ScreenShot](MX10TANSA.png)

**TANSA: THE ARTIFICIAL AI BOT**

We have the idea of making our own artificial AI bot called TANSA. We want to write the answers of the AI bot as text lines in a JSON file together with sound files of us reading the lines aloud. All in the group will use their voices to record the lines. This is to include both male and female voices and thereby to contribute to the equality debate in digital culture. This is also to break with the manipulation of how consumers perceive information given from different genders. Male voices have a tendency to carry more authority. Female voice tends to be more persuasive.
We are not skilled enough to make a real interaction with AI. So, one of the main challenges of this program is that we want to imitate a mic input interaction with our TANSA bot giving the illusion of the bot answering your question. To do that, we might have to make a conditional statement saying that the button has to be pressed for at least 2 seconds before the bot fetches an answer. While pressing the mic button a throbber will indicate that the button is pressed. We are going to use the boolean statement of mousePressed meaning that the mic button has to be held down and then released before the JSON data is called. Another option for constraining the JSON data access is to let the mic input decide it. So, if the program captures a specific volume level the JSON data is accessed and fetched. 
We have called our AI bot ‘TANSA’ which is an acronym for There Are No Stupid Answers. We want to make a critical statement with this program, by somewhat ridiculing the way we tend to lay our trust in machines. So, we basically want people to reconsider their use of machines and make them engage with machines with a more critical sense, not taking everything for gospel truth.


![ScreenShot](MX10automation.png)

**The sound of automation**

When is art really art? Does it have to be created by a human or can a machine generate art? We want to test this with a program that generates a piece of digital art combined with music. We as creators of the program will set some rules for the program to execute and thereby give the machine the possibility to create a piece of art on its own. This means that there are still humans behind it, making us the overall authors. 

We want to create a program that generates art inspired by Mathias’ mini_ex7 where he created a generative art piece consisting of ellipses and frames of other shapes appearing when the ellipses would reach specific points. We have used similar design elements by making ellipses appear from the middle of the canvas, moving randomly and creating a randomly generated pattern. But we have redesigned the behavior of it crossing the borders. When it reaches either the bottom, top, right or left border a sound of a chord will play and a geometric shape will appear, continuously growing in size. 
One of the challenges with this idea is that we are unsure of how to implement the sound output when the ellipses reach the borders


____


**Individual: How is this flowchart different from the one that you had in #3 (in terms of the role of a flowchart)?**

The purpose of the flowchart for my miniex 6 is primarily to give a visualization on how the program works and in what order processes occur. It is drawn with the miniex 6 source code as a foundation, and in that sense, it is more accurately showing how the program works. The flowcharts of our miniex10 ideas were made with the purpose of sketching and inspiring. They are drawn as blueprints or recipes for us to use/follow when writing the program. Therefore, flowcharts bear different possibilities depending on the desired purposes.

**Individual: If you have to bring the concept of algorithms from flowcharts to a wider cultural context, how would you reflect upon the notion of algorithms? (see if you could refer to the text and articulate your thoughts?)**

Algorithms and flowcharts can both be seen as recipes for instructions to be executed and rules to be followed which is why one can say they are deeply intertwined with each other. They both strive for giving the most organized visualization possible of the tasks to be conducted, while placed in the most efficient order. However, flowcharts are primarily used as a medium for communication between humans. It is a way of grasping the complexity of algorithms. So, flowcharts are the comprehendible way for humans to understand algorithms. *“From a technical standpoint, creating an algorithm is about breaking the problem down as efficiently as possible, which implies a careful planning of the steps to be taken and their sequence.”* (Bucher 2018, p 6) As suggested here, the creation of algorithms is done very thoughtfully with the goal of obtaining the wanted results in the most efficient way, and this exact process is quite similar when wanting to make flowcharts. You create a path under a specific set of rules with the aim of achieving a wanted outcome. Similar processes can be seen in social and cultural contexts, where specific rules, as well as unspecified rules, must be followed. Through governments and politics, all societies are basically built upon algorithms with various rules and laws to be obeyed, tasks in the form of labor and education to maintain the social- and economic infrastructure. They are designed to make everything run smoothly and generate a good life of its citizens. These social algorithms are basically what define societies with everything they consist of. Therefore, they are important to acknowledge and act upon.


**References**

Bucher, Taina. "The Multiplicity of Algorithms". *If...THEN: Algorithmic Power and Politics*, Oxford University Press, 2018

