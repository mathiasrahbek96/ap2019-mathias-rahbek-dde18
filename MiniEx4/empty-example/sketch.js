var buttonOn;
var buttonOff;
var mic;
var capture;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(250, 170, 100);
  mic = new p5.AudioIn();
  mic.start();


  buttonOn = createButton('ON');
  buttonOn.position(450, 600);
  buttonOn.style("font-size", "1em");
  buttonOn.style("text-shadow", "0 -1px 0 rgba(0,0,0.02)");
  buttonOn.style("color", "rgba(250)");
  buttonOn.style("border-radius", "10px");
  buttonOn.style("background", "rgba(200,200,200)");

  buttonOff = createButton('OFF');
  buttonOff.position(945, 600);
  buttonOff.style("font-size", "1em");
  buttonOff.style("text-shadow", "0 -1px 0 rgba(0,0,0.02)");
  buttonOff.style("color", "rgba(250)");
  buttonOff.style("border-radius", "10px");
  buttonOff.style("background", "rgba(200,200,200)");
}

function closecam() { //function for turning off webcam
  capture.hide();
}

function startcam() { //function for starting webcam
  capture = createCapture();
  capture.size(600, 400);
  capture.position(420, 185);
}


function keyPressed() { //scenery mode
  if (keyCode === 32) {
    background(100, 80, 50)
  } else {
    background(250, 170, 100);
  }
}

function draw() {

  buttonOn.mousePressed(startcam);
  buttonOff.mousePressed(closecam);

  // /// SPEAKERS ///

  var vol = mic.getLevel();


  //left speaker
  push()
  fill(0)
  strokeWeight(4);
  beginShape() //wire
  noFill()
  curveVertex(240, 540);
  curveVertex(240, 540);
  curveVertex(290, 560);
  curveVertex(350, 540);
  curveVertex(450, 550);
  curveVertex(450, 550);
  endShape();
  pop()
  fill(100);
  rect(150, 280, 140, 350, 20, 14); //left cabinet
  fill(40);
  ellipse(220, 400, 50, 50); //little membrane
  ellipse(220, 520, 100, 100); //big membrane
  noFill()
  ellipse(220, 400, 100 * vol, 100 * vol); //little membrane
  ellipse(220, 520, 200 * vol, 200 * vol); //big membrane

  //right speaker
  push()
  fill(0)
  strokeWeight(4);
  beginShape() //wire
  noFill()
  curveVertex(950, 540);
  curveVertex(950, 540);
  curveVertex(1030, 560);
  curveVertex(1100, 540);
  curveVertex(1200, 550);
  curveVertex(1200, 550);
  endShape();
  pop()
  fill(100);
  rect(1150, 280, 140, 350, 20, 14); //right cabinet
  fill(40);
  ellipse(1220, 400, 50, 50); //little membrane
  ellipse(1220, 520, 100, 100); //big membrane
  noFill()
  ellipse(1220, 400, 100 * vol, 100 * vol); //little membrane
  ellipse(1220, 520, 200 * vol, 200 * vol); //big membrane

  /// TV SCREEN ///

  //antenna
  push();
  strokeWeight(4);
  line(width / 2, 150, 500, 40) //left
  line(width / 2, 150, 950, 40); //right
  pop();

  fill(100);
  ellipse(width / 2, 155, 150, 60);

  //screen frame
  fill(100);
  rect(420, 150, 600, 490, 10);

  //screen window
  fill(0);
  rect(450, 182, 539, 404);

  //feet
  push();
  fill(200, 100, 0);
  quad(470, 640, 470, 690, 490, 690, 510, 640); //left foot
  quad(935, 640, 955, 690, 975, 690, 975, 640); //right foot
  pop();
}
