var NYTFont;
var apiRef;
var input;
var api ="https://api.nytimes.com/svc/search/v2/articlesearch.json?q=";
var apiKey = "&api-key=w6mbi5pA1mU0JmG2m5PCO6G4tltU6cYM";

//loading of the font and reference image
function preload(){
  NYTFont = loadFont("data/Chomsky.otf")
  apiRef = loadImage("data/apiref.png")
}
//Canvas and button for searchbar
function setup(){
  createCanvas(windowWidth, 1700);
  var button = select('#Search');
  button.mousePressed(updateSearchWord);
  input = select('#searchWord');
}
//update of the search word of choice
function updateSearchWord(){
  var url = api + input.value() + apiKey;
  loadJSON(url, gotData);
  refresh();
}

//accessing NYT api
function gotData(data){
  var articles = data.response.docs
  for(var i = 0; i < articles.length; i++){
  textFont('TimesNewRoman')
  textSize(30);
  text(articles[i].headline.main, 80,200+i*150)
  textSize(20);
  text(articles[i].snippet, 80, 235+i*150, windowWidth-200);
  }
}
//Headline
function draw() {
  background(240);
  image(apiRef, windowWidth-70,1670)

  push();
  fill(0)
  textFont(NYTFont)
  textSize(90)
  textAlign(LEFT)
  text('The New York Times Replica', 50,100)
  pop();
//making it able to load a new search on top of the old search
  textSize(30)
  gotData(data);
  noLoop();
}
//redrawing the background, causing the old search to "dissapear" underneith a new background, allowing the new search to be shown without the old search
function refresh(){
  loop();
}
//this lets the "enter" key to update the search word without having to press the button
function keyPressed(){
  if(keyCode===13){
    updateSearchWord();
  }
}
