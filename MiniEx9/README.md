![ScreenShot](example.png)

[RUNME](https://cdn.staticaly.com/gl/mathiasrahbek96/ap2019-mathias-rahbek-dde18/raw/master/MiniEx9/empty-example/index.html)

**Our program**

This program has been developed by Group 9(Kathrine, Lise, Mathias, and Noah). 
The title of our program is “The New York Times Replica”. 
The program is basically a search engine - when you first enter the program you’re met by a blank page, as the program demands that you pick a word and type it into the search input. Afterward, you either press the enter key or click the 'search' button. And then you are met by up to 10 headlines of articles associated with your search word of choice. The program is then constrained to only allow you to view the headline and the snippet of the article, but not allowing you to explore the articles further. 
The way it was possible to access these articles, was by using The New York Times’ “Article Search API”, allowing you entry to their archive. 

**The process**

The week before we started coding the program we sat down in the group and had a short planning session. In this session, we discussed different topics that we would be able to work with using an API, but quickly settled on the idea of using an API from a newspaper/media and make use of it as a search tool.
Later the same week we got together in our group and we started the coding process. We sought out The New York Times as our media and found the API. We chose to go with the New York Times because we wanted to work with an internationally known and respected newspaper with a big archive of articles. 
Our initial idea was actually to work with two APIs and we had chosen BBC News as the second API, as we wanted to be able to compare articles from two different news mediums while sharing the same query. This we, unfortunately, were not able to move forward with, as we struggled a lot with just make the New York Times API work as we wanted. This was not something we saw as a defeat though - it is just what happens sometimes in your coding process when you set high ambitions. We just adjusted our idea to what we as a team would be able to make work, still knowing what our code meant. 

**Understanding of the provided data** 

Moving forward in our process and onto the data we were able to access through the Article Search API, this was to us easily understandable in the way that, firstly, we had chosen a text-based outcome, but also, secondly, the query word plays a role in the articles displayed. What is not as easily understandable is the order in which the articles are shown. We hadn’t made any criteria to make the program sort the articles in a specific way. The sorting of the articles is not by date, as we have tried to look them up and found out that it was not the case. This is a question we have left unanswered. But we discussed that it maybe was sorted by the most read/shown articles. 
Reviewing the JSON file provided by the API. This is easy to navigate through, as the names are deliberately given thereby making the values easy to make sense of. But that is still not answering our question. 

**APIs in a digital culture**

APIs are essentially the backbone of the internet, providing access for relevant information which you as the user desire. They act as the bridges for you to enter various domains and information sources. This means that we in a digital culture are privileged to gain access to whatever we want at rapid speed and efficiency. As Winnie Soon and Eric Snodgrass mentions in their paper API practices and paradigms, APIs acts as links and facilitators for an exchange of data and information.
 
*“At a basic level, APIs do the work of exposing and making available the resources of particular programmatic components so that they can be accessed by other programmatic components. APIs typically do so with a view to facilitating forms of exchange and interoperability between these components and the corresponding applications and/or larger systems within which they are situated.”* (Snodgrass and Soon, p. 5)
 
This means that APIs are very important operators in the World Wide Web for everything to be linked together. They can perhaps be seen as shortcuts for information and data. They are in that manner of very high value because of their operational processes of giving access and linking various systems together. We now live in a world where everything has to be profitable, efficient, and of high quality. And for you to be able to live in the 21st century, high endurance is required for you to keep up with everything that you associate yourself with. You can’t just exist here and now in real life. You also have to live digitally. And APIs are feeding this digital life by supplying us with data and information and keeping us updated. This isn’t necessarily meant in a negative way because APIs are really great companions when seeking specific information. It is acting as the waiter that attend on corresponding queries and orders between you and the kitchen chef. Without help from APIs, the world would run drastically slower because of their great impact on applications and general digital infrastructure in larger systems which they are interfering with, as Snodgrass and Soon mentioned above. 

**Questions in relation to web API:**
 
If more companies chose to limit access to their APIs so as Twitter did, how will it affect the development of third-party software? 

Let’s take a look at Google Maps and let's say they decided to fully limit the access in regards to their API, will this eventually lead to more competitors in the same area or how would it affect the navigation business?


**References**

Snodgrass, Eric, and Winnie Soon. “API Practices and Paradigms: Exploring the Protocological Parameters of APIs as Key Facilitators of Sociotechnical Forms of Exchange.” First Monday, vol. 24, no. 2, Feb. 2019. firstmonday.org, doi:10.5210/fm.v24i2.9553. (Accessed 8 April, 2019)

