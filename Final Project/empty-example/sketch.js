//audio capture
var mic;

//audio output
var tansaVoice = new p5.Speech;
tansaVoice.setLang('en-UK');
tansaVoice.setVoice('Google UK English Male');
var quest = false;

//images
var imgMic;
var imgLogo;

//loading throbber
var k = 99,
  r = 70;

//text font
var ralewayFont;

//JSON
var tansasBrain
var replies = [];

//
var space = false;

function preload() {
  tansasBrain = loadJSON('data/TANSASbrain.json')
  imgMic = loadImage('data/micicon.png');
  imgLogo = loadImage('data/tansalogo.png');
  ralewayFont = loadFont('data/Raleway-Light.ttf')
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  mic = new p5.AudioIn();
  mic.start();
}


function draw() {
  var vol = mic.getLevel();
  background(255);

  //visuals
  fill(2, 23, 142);
  noStroke()
  ellipse(windowWidth / 2, windowHeight / 4 * 3, 100);
  //voice capture constrain
  if (vol > 0.30) {
    quest = true;
  }


  //interaction
  if (keyIsDown(32)) {
    ellipse(windowWidth / 2, windowHeight / 4 * 3, 100 + (50 * vol));
    //voice capture constrain
    if (quest == false) {
      textAlign(CENTER)
      text('I cannot hear you', windowWidth / 2, windowHeight / 2)
    }
  }

  //boolean giving access
  if (space == true) {
    spaceReleased();
  }

  //MicIcon
  imageMode(CENTER);
  image(imgMic, windowWidth / 2, windowHeight / 4 * 3, imgMic.width / 5, imgMic.height / 5);
  image(imgLogo, windowWidth / 2, windowHeight / 4 - 50, imgLogo.width / 5, imgLogo.height / 5);

  //Text + textfont
  textFont(ralewayFont);
  textSize(20);
  textAlign(CENTER);
  text('Hold space to ask', windowWidth / 2, windowHeight / 2 + 320);
  textSize(12);
  text('Press N to ask a new question', windowWidth / 2, windowHeight / 2 + 350);
}

//if the voice is captured adequately the quest boolean is equals true and space becomes "disabled"
function keyReleased() {
  if (keyCode === 32 && quest == true) {
    space = !space;
  }
}

//if space is true various functions are called
function spaceReleased() {
  loading();
  setTimeout(createReplies, 5000);
  setTimeout(hideThrobber, 5000)
  setTimeout(noLoop, 5000)
}

//THROBBER
function loading() {
  push();
  translate(width / 2, height / 4 * 3);
  rotate(PI / 2);
  noFill();
  stroke(255);
  ellipse(windowWidth / 2, windowHeight / 4 * 3, 2 * r, 2 * r)
  for (let i = 1; i < 2; i++) {
    let ang = radians(-180 * cos(radians(k + i))); //radians describes a circular arc that draws from the center of a circle and has the length of the raidus. It is 180/PI degrees
    let x = r * cos(ang);
    let y = r * sin(ang);
    noStroke();
    fill(2, 23, 142);
    if ((k + i) < 182) ellipse(x, y, 20, 20);
  }
  if (k < 180) {
    k += 1;
  } else {
    k = 0
  };
  pop();
}

//The throbber is hidden when the timer is up
function hideThrobber() {
  //white ellipse in background
  noStroke()
  fill(255)
  ellipse(windowWidth / 2, windowHeight / 4 * 3, 180)
  //blue ellipse around the mic img, acting as resizer
  fill(2, 23, 142);
  ellipse(windowWidth / 2, windowHeight / 4 * 3, 100);
  //new img
  imageMode(CENTER);
  image(imgMic, windowWidth / 2, windowHeight / 4 * 3, imgMic.width / 5, imgMic.height / 5);
}

//FETCHING A REPLY
//Takes the data from our JSON and loads it into an array (replies[])
//Then it chooses one random object from the JSON which we then load into the variable 'reply', that is written and read aloud
function createReplies() {
  for (var i = 0; i < tansasBrain.answers.length; i++) {
    var reply = random(tansasBrain.answers)
    replies.push(reply);
  }
  for (var i = 0; i < 1; i++) {
    var reply = replies[i];

    //Text reply
    fill(2, 23, 142);
    textFont(ralewayFont);
    textSize(20)
    textAlign(CENTER)
    text(reply, windowWidth / 2, windowHeight / 2);

    //Voice reply
    tansaVoice.speak(reply)
  }
}

//when pressing keyCode 78 = n the refreshProgram is called
function keyPressed() {
  if (keyCode === 78) {
    refreshProgram();
  }
}

//function to refresh the program when called
function refreshProgram() {
  location.reload();
}
